import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";

export function showFighterDetailsModal(fighter) {
  const title = "Fighter info";
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source: image } = fighter;

  const fighterDetails = createElement({
    tagName: "div",
    className: "modal-body",
  });
  const nameElement = createElement({
    tagName: "span",
    className: "fighter-name",
  });
  const attackElement = createElement({
    tagName: "span",
    className: "fighter-attack",
  });
  const defenseElement = createElement({
    tagName: "span",
    className: "fighter-defense",
  });
  const healthElement = createElement({
    tagName: "span",
    className: "fighter-health",
  });
  const imageElement = createElement({
    tagName: "img",
    className: "fighter-image",
  });

  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  attackElement.innerText = attack;
  defenseElement.innerText = defense;
  healthElement.innerText = health;
  imageElement.src = image;

  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(imageElement);

  return fighterDetails;
}
