import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  // show winner name and image
  const title = "Winner";
  const bodyElement = fighter.name;

  showModal({ title, bodyElement });
}
