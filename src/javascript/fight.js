export function fight(firstFighter, secondFighter) {
  // return winner
  let firstHealth = firstFighter.health;
  let secondHealth = secondFighter.health;
  while (firstHealth > 0 && secondHealth > 0) {
    secondHealth -= getDamage(firstFighter, secondFighter);
    firstHealth -= getDamage(secondFighter, firstFighter);
  }

  return firstHealth > secondHealth ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
  // damage = hit - block
  let damage = getHitPower(attacker) - getBlockPower(enemy);

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = getRandNum();
  let hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = getRandNum();
  let blockPower = fighter.defense * dodgeChance;
  return blockPower;
}

function getRandNum() {
  return Math.random() + 1;
}
